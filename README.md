# LidarView Deep Learning Plugins

This repo contains ParaView/LidarView python plugins to run deep learning 
models on point clouds.

## Currently supported frameworks
- MMDetection3d: https://gitlab.kitware.com:keu-computervision/ml/mmdetection3d

## Installation

For a complete installation guide of each framework, check the associated `INSTALL.md` file.

## Using the plugin

### Plugin installation
To add a python plugin to ParaView/LidarView, follow the simple steps below:

- Open `Advance` > `Tools` > `Manage Plugins`...
- Select `Load New and` open the python file

The plugin is loaded and you can enable `Auto Load` under the plugin name if you want it available for all your future sessions.

### Running

Once the plugin is installed/loaded:
- If the plugin is a reader, it will show up in file open options (along with other file formats in the list).
- In case of a `filter`, you will see the filter in the list of Filters (or Ctrl+Space to search for it).
- Writers will appear as options in `Save Data`

You can also use a convenient script that will automatically create and run a pipeline to process input data (usually from disk).
In that case, make sure to first enable the python shell view in `View` > `Python Shell`. Now, just hit the `Run Script` button and select the python script file.

### Headless mode

It is completely possible to run the python script on the command line and without the LidarView UI. 
This is really convenient because you don't need to install any plugin as it wil be loaded automatically by the script.
Simply use the integrated python executable `lvpython`, for example:
```
lvpython /path/to/*_script.py
```
All python integrated modules are available like `pdb` for debugging, or `cProfile` for profiling.

## External ressources
This repo has been featured in the Kitware's blog:

https://www.kitware.com/ai-point-cloud-deep-learning-in-paraview-and-lidarview/

https://www.kitware.com/3d-point-cloud-object-detection-in-lidarview/

The blog posts contains visuals and usage examples.
