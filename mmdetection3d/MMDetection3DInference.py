# =============================================================================
#
# Copyright (c) Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# =============================================================================

"""
This module contains a Paraview filter that runs a 3d object detection model
using MMDetection3d on a polydata object.
This requires MMDetection3d installed on the python used by paraview
See https://github.com/open-mmlab/mmdetection3d
"""

import warnings
from copy import deepcopy
import time

# lidarview related imports
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkFiltersSources import vtkOutlineSource

# vtk-numpy integration
from vtk.numpy_interface import dataset_adapter as dsa
import numpy as np

from paraview.util.vtkAlgorithm import *
from paraview.web import venv

try:
    import mmcv
    import torch
    from mmdet3d.apis import init_model

    from mmdet3d.core.bbox import get_box_type
    from mmdet3d.core.points import get_points_type
    from mmdet.datasets.builder import PIPELINES
    from mmdet3d.datasets.pipelines import Compose

except ImportError as e:
    print("""MMDetection3d modules could not be loeaded.
    Please install MMDetection3d in the python3.7 environment used by lidarview
    (https://github.com/open-mmlab/mmdetection3d)""")
    raise e


# =============================================================================
# New mmdet3d pipeline module to convert from lidarview data
# =============================================================================
@PIPELINES.register_module()
class PreprocessRawPoints(object):
    """Load Points From Polydata.

    Args:
        load_dim (int): The dimension of the loaded points.
            Defaults to 6.
        coord_type (str): The type of coordinates of points cloud.
            Available options includes:
            - 'LIDAR': Points in LiDAR coordinates.
            - 'DEPTH': Points in depth coordinates, usually for indoor dataset.
            - 'CAMERA': Points in camera coordinates.
        shift_height (bool): Whether to use shifted height. Defaults to False.
    """

    def __init__(self,
                 coord_type,
                 shift_height=False):
        self.shift_height = shift_height
        assert coord_type in ['CAMERA', 'LIDAR', 'DEPTH']
        self.coord_type = coord_type

    def __call__(self, results):
        """Call function to load points data from file.

        Args:
            results (dict): Result dict containing point clouds data.

        Returns:
            dict: The result dict containing the point clouds data. \
                Added key and value are described below.

                - points (np.ndarray): Point clouds data.
        """
        points = results['points']
        attribute_dims = None

        if self.shift_height:
            floor_height = np.percentile(points[:, 2], 0.99)
            height = points[:, 2] - floor_height
            points = np.concatenate([points, np.expand_dims(height, 1)], 1)
            attribute_dims = dict(height=3)

        points_class = get_points_type(self.coord_type)
        points = points_class(
            points, points_dim=points.shape[-1], attribute_dims=attribute_dims)
        results['points'] = points

        return results

    def __repr__(self):
        """str: Return a string that describes the module."""
        repr_str = self.__class__.__name__ + '('
        repr_str += 'shift_height={}, '.format(self.shift_height)
        return repr_str


# =============================================================================
# Result conversion to lidarview format
# =============================================================================

def bboxCornersToVtkOutlineCorners(box):
    """ Convert bbox corners from mmdet3d convention to vtkOutlineSource convention
        5 -------- 6                    6 -------- 7
       /|         /|                   /|         /|
      1 -------- 2 .        --->      4 -------- 5 .
      | |        | |                  | |        | |
      . 4 -------- 7                  . 2 -------- 3
      |/         |/                   |/         |/
      0 -------- 3                    0 -------- 1
    """
    out_box = box[[0, 3, 4, 7, 1, 2, 5, 6], :]

    return out_box

def CreateBBox3D(box):
    boxSource = vtkOutlineSource()
    boxSource.SetBoxTypeToOriented()
    corners = bboxCornersToVtkOutlineCorners(box)
    boxSource.SetCorners(corners.flatten())
    boxSource.Update()

    return boxSource.GetOutput()


def PopulateMultiBlockDataSet(outData, boxes3d, categories, scores):
    nb_boxes = len(scores)
    outData.SetNumberOfBlocks(nb_boxes)
    for ii in range(nb_boxes):
        box3d = CreateBBox3D(boxes3d[ii, ...])
        box3d = dsa.WrapDataObject(box3d)
        box3d.FieldData.append(categories[ii], 'category')
        box3d.FieldData.append(scores[ii], 'score')
        outData.SetBlock(ii, box3d.VTKObject)


# =============================================================================
# Lidarview filter
# =============================================================================

@smproxy.filter(label='MMDet3dInference')
@smproperty.input(name="Input", port_index=0)
class MMDet3dInferenceFilter(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self,
                                        nInputPorts=1,
                                        nOutputPorts=1,
                                        inputType="vtkPolyData",
                                        outputType="vtkMultiBlockDataSet")
        self.cfg = None
        self.ckpt = None
        self.model = None
        self.detection_threshold = 0.1
        self.needInitialization = True
        self.use_intensity = True
        self.use_centered_pc = True

    def InitializeModel(self):
        if (not self.cfg) or not (self.ckpt):
            warnings.warn("In order to initialize the model, please provide a config and a weight files.\n" +
                  "The output of this filter will remain empty until the model could be initialized.")
            return False

        if self.model is not None:
            del self.model

        # TODO: enable choosing device
        self.model = init_model(self.cfg, self.ckpt, device='cuda:0')
        self.needInitialization = False

        return True

    def RequestData(self, request, in_info, out_info):
        in_data = self.GetInputData(in_info, 0, 0)
        out_data = self.GetOutputData(out_info, 0)

        in_data = dsa.WrapDataObject(in_data)
        raw_points = in_data.Points
        intensity = in_data.PointData["intensity"]
        points_offset = (np.median(raw_points, axis=0) if self.use_centered_pc
                         else np.zeros(3))

        points = raw_points - points_offset
        if self.use_intensity:
            points = np.column_stack((points, intensity))

        if self.needInitialization:
            modelCouldBeInitialized = self.InitializeModel()

            if not modelCouldBeInitialized:
                return 1

        box_type_3d, box_mode_3d = get_box_type(self.cfg.data.test.box_type_3d)
        data = dict(
            points=points,
            box_type_3d=box_type_3d,
            box_mode_3d=box_mode_3d,
            img_fields=[],
            bbox3d_fields=[],
            pts_mask_fields=[],
            pts_seg_fields=[],
            bbox_fields=[],
            mask_fields=[],
            seg_fields=[]
        )
        test_pipeline = deepcopy(self.cfg.data.test.pipeline)
        # print(self.cfg.data.test.pipeline)
        loader_pipeline = dict(type='PreprocessRawPoints', coord_type='LIDAR')
        pipelines_to_skip = ['LoadPointsFromFile',
                             'LoadPointsFromMultiSweeps',
                             'LoadPointsFromPandasetFile']
        test_pipeline = [p for p in test_pipeline
                         if p['type'] not in pipelines_to_skip]
        test_pipeline = [loader_pipeline] + test_pipeline
        test_pipeline = Compose(test_pipeline)
        data = test_pipeline(data)

        data = mmcv.parallel.collate([data], samples_per_gpu=1)

        device = next(self.model.parameters()).device  # model device
        if next(self.model.parameters()).is_cuda:
            # scatter to specified GPU
            data = mmcv.parallel.scatter(data, [device.index])[0]
        else:
            # this is a workaround to avoid the bug of MMDataParallel
            data['img_metas'] = data['img_metas'][0].data
            data['points'] = data['points'][0].data

        t = time.time()
        # forward the model
        with torch.no_grad():
            results = self.model(return_loss=False, rescale=True, **data)
        print("inference_time = ", time.time() - t)
        results = [r['pts_bbox'] if ('pts_bbox' in r.keys()) else r for r in results]

        # Consider only the first result because inference is done with a
        # size 1 batch
        result = results[0]

        indices_within_threshold = result["scores_3d"] >= self.detection_threshold

        boxes = result["boxes_3d"].corners[indices_within_threshold].cpu().numpy()
        boxes = boxes + points_offset
        categories = result["labels_3d"][indices_within_threshold].cpu().numpy()
        scores = result["scores_3d"][indices_within_threshold].cpu().numpy()

        PopulateMultiBlockDataSet(out_data, boxes, categories, scores)

        return 1

    @smproperty.stringvector(
            name="ConfigFile",
            number_of_elements=1
    )
    @smdomain.filelist()
    @smhint.filechooser(extensions="py", file_description="MMDet3d configuration file")
    def SetConfig(self, filename):
        if filename:
            self.cfg = mmcv.Config.fromfile(filename)
            self.needInitialization = True

        self.Modified()

    @smproperty.stringvector(
            name="ModelWeightsFile",
            number_of_elements=1
    )
    @smdomain.filelist()
    @smhint.filechooser(extensions="pth", file_description="pytorch weights file")
    def SetModelWeights(self, filename):
        if filename:
            self.ckpt = filename
            self.needInitialization = True

        self.Modified()

    @smproperty.doublevector(name="DetectionThreshold", default_values=[0.1])
    def SetDetectionThreshold(self, th):
        self.detection_threshold = th

        self.Modified()

    # @smproperty.doublevector(name="UseIntensity", default_values=[1])
    @smproperty.xml("""
        <IntVectorProperty name="UseIntensity"
            number_of_elements="1"
            default_values="1"
            command="SetUseIntensity">
            <BooleanDomain name='bool'/>
        </IntVectorProperty>
        """)
    def SetUseIntensity(self, val):
        self.use_intensity = val

        self.Modified()

    @smproperty.xml("""
        <IntVectorProperty name="UseCenteredPointCloud"
            number_of_elements="1"
            default_values="1"
            command="SetUseCenteredPointCloud">
            help="Shift the point cloud around its median point before running
                  Inference"
            <BooleanDomain name='bool'/>
        </IntVectorProperty>
        """)
    def SetUseCenteredPointCloud(self, val):
        self.use_centered_pc = val

        self.Modified()
