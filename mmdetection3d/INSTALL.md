# Linux guide to enable mmdetection3d object detection within Paraview/Lidarview

This guide was written and tested on Ubuntu 20.04, though we expect it to work on earlier version.

1. Install nvidia dependencies
* NVIDIA driver
```
sudo rm /etc/apt/sources.list.d/cuda*
sudo apt remove --autoremove *cuda*
sudo apt remove --autoremove *nvidia*
sudo apt remove --autoremove *cudnn*
sudo apt update
```

* Install nvidia driver and cuda driver 11.7 using nvidia-toolkit
From nvidia [documentation](https://developer.nvidia.com/cuda-11-7-0-download-archive?target_os=Linux&target_arch=x86_64&Distribution=Ubuntu&target_version=20.04&target_type=deb_network)

```
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/3bf863cc.pub
sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
sudo apt-get update
sudo apt-get -y install cuda-11.7
```

You will need to reboot for driver to be properly loaded. 
```
sudo reboot
```

For more information ont the difference between cuda driver and cuda runtime you can check [this page](https://docs.nvidia.com/cuda/cuda-runtime-api/driver-vs-runtime-api.html).

* Verify installation
```
nvcc --version
nvidia-smi
```

* Install cudnn 8.5.0

```
wget https://developer.nvidia.com/compute/cudnn/secure/8.5.0/local_installers/11.7/cudnn-linux-x86_64-8.5.0.96_cuda11-archive.tar.xz
```
You can now install `cudnn` on your system using [this doc](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html#installlinux-tar).
Don't forget to check the installation with [this section](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html#verify).

If you have any issues, check the [official documentation](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html)

* Remove apt repository to disable automatic update

This is an important step as there are some issues with the [ubuntu kernel update and nvidia drive incompability](https://forums.developer.nvidia.com/t/failed-to-initialize-nvml-driver-library-version-mismatch/190421).

```
sudo add-apt-repository --remove "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
sudo apt update
```

2. Make sure you have python 3.8 installed on your system

If you don't have python 3.8 by default, you can install it in another alternative directory.
First make sure to install the following:
```
sudo apt install libssl-dev libncurses5-dev libsqlite3-dev libreadline-dev libtk8.6 libgdm-dev libdb4o-cil-dev libpcap-dev
```
Download the latest from [this index](https://www.python.org/ftp/python/) and untar it to any location you want (best in `/usr/local/src`).
Now you can build and altinstall (https://docs.python.org/3/using/unix.html#building-python):
```
./configure
make
make altinstall
```

2. Compile lidar view with python from system

    Follow instructions [here](https://gitlab.kitware.com/LidarView/lidarview-superbuild/-/blob/master/README.md) but replace the `cmake` configuration line by this one:
```
cmake ../lvsb -GNinja -DCMAKE_BUILD_TYPE=Release -DENABLE_paraviewweb=ON -DUSE_SYSTEM_python3=ON
```

Make sure that the correct python is targeted by cmake.
```
cd /path/to/lvsb/build/pvsb/superbuild/paraview/build/
ccmake .
```
And check the following variables: `Python3_EXECUTABLE`, `Python3_INCLUDE_DIR` and `Python3_LIBRARY`.

If you have further issues you can check [this thread](https://discourse.paraview.org/t/quick-guide-to-using-pytorch-in-paraview/9037).

3. Create/activate a [python venv](https://docs.python.org/3/library/venv.html) and install `pytorch`

First we will create a python venv
```
python3.8 -m venv ~/.virtualenvs/lv-custom
source ~/.virtualenvs/lv-custom/bin/activate
cd ~/.virtualenvs/lv-custom

pip install -U pip setuptools requests pillow numpy==1.23.5 open3d
```

We want to install pytorch `1.13.1` among torchvision `0.14.1`.
Please check the corresponding version that match your cuda (cuda version `11.7` was used here).
```
pip install https://download.pytorch.org/whl/cu117/torchvision-0.14.1%2Bcu117-cp38-cp38-linux_x86_64.whl
```

6. Compile mmdet3d dependencies

The administrators of `mmdet3d` provides some pre-compiled binaries, so we could think it is faster and easier to [install directly](https://mmcv.readthedocs.io/en/latest/get_started/installation.html#install-with-pip) dependencies from pip. However from our experience, it is easier to re-compile locally everything to make sure the stack is compatible with the current pytorch version.
```
mkdir ~/.virtualenvs/lv-custom/src && cd ~/.virtualenvs/lv-custom/src
git clone https://github.com/open-mmlab/mmcv && cd mmcv && git checkout v1.7.0
pip install -r requirements/build.txt && MMCV_WITH_OPS=1 pip install --no-cache -e .
cd ../ && git clone https://github.com/open-mmlab/mmdetection && cd mmdetection && git checkout v2.28.2
pip install --no-cache -e .
cd ../ && git clone https://github.com/open-mmlab/mmsegmentation && cd mmsegmentation && git checkout v0.30.0
pip install --no-cache -e .
```

7. Install mmdet3d

All instructions comes from the [mmlab detection3d documentation](https://mmdetection3d.readthedocs.io/en/latest/getting_started.html#best-practices).

First, we clone the repository
```
cd ../ && git clone -b pandaset_integration_update git@gitlab.kitware.com:keu-computervision/ml/mmdetection3d.git && cd mmdetection3d
```
Now install
```
pip install -r requirements.txt
pip install -r requirements/pandaset.txt
pip install --no-cache -e .
```

8. Prepare checkpoints and data

An inference model needs to load its checkpoint (a save state with frozen parameter), additionnaly in `mmlab` packages, the dataset that was used to train the model needs also to be there.
Basic checkpoints for `mmdetection3d` are available [here](https://mmdetection3d.readthedocs.io/en/latest/model_zoo.html).
Instructions for custom data can be found [there](https://mmdetection3d.readthedocs.io/en/latest/tutorials/customize_dataset.html) if you need to train a new model.
Once you have the checkpoints and data, create a symlink in the root repository:
```
cd ~/.virtualenvs/lv-custom/src/mmdetection3d
ln -s PATH/TO/DATASET data/
ln -s PATH/TO/CHECKPOINT_DIR checkpoints
```

# Testing

Make you have loaded the `venv`.
```
source ~/.virtualenvs/lv-custom/bin/activate
```
## torch and torchvision
Check if `torch` and `torchvision` is correctly installed with cuda support using the following:
```
python -c "import torch; import torchvision; print(torch.__version__); print(\"cuda: {}\".format(torch.cuda.is_available())); print(torchvision.__version__)"
```
It is also recommended to execute the [following script](https://pytorch.org/tutorials/beginner/pytorch_with_examples.html#pytorch-tensors) inside python for further testing.

## mmdetection3d

The `mmdetection3d` package provides a sample script that you should try.

We first need to download the model checkpoints for inference, all available models are available [here](https://mmdetection3d.readthedocs.io/en/latest/model_zoo.html).
There are several variants for each model, for example for SECOND you can select which training data was used, or the floating precision.
Here we will download SECOND model using [this link](https://download.openmmlab.com/mmdetection3d/v0.1.0_models/fp16/hv_second_secfpn_fp16_6x8_80e_kitti-3d-3class/hv_second_secfpn_fp16_6x8_80e_kitti-3d-3class_20200925_110059-05f67bdf.pth).
```
wget https://download.openmmlab.com/mmdetection3d/v0.1.0_models/fp16/hv_second_secfpn_6x8_80e_kitti-3d-3class/hv_second_secfpn_fp16_6x8_80e_kitti-3d-3class_20200925_110059-05f67bdf.pth
```

Now let's run the inference, on kitti dataset
```
cd ~/.virtualenvs/lv-custom/src/mmdetection3d
python demo/pcd_demo.py demo/data/kitti/kitti_000008.bin configs/second/hv_second_secfpn_6x8_80e_kitti-3d-3class.py checkpoints/hv_second_secfpn_6x8_80e_kitti-3d-3class_20200925_110059-05f67bdf.pth --show
```

It should open a new window with the point cloud and bounding boxes prediction.
The resulting files are in `./demo/*.obj`.
You are invited to test other models as well, following [this section](https://mmdetection3d.readthedocs.io/en/latest/demo.html#demo).

# Lidarview post-installation step

Now that the library is functionning, you must ensure that you can load the `venv` inside `Lidarview`.
You have two choices from there that are listed in [the discourse](https://discourse.paraview.org/t/quick-guide-to-using-pytorch-in-paraview/9037).
We recommend you to use the `PV_VENV` variable instead of the argument option because it is not working with Lidarview.

Either define the variable each time you run Lidarview:
```
PV_VENV="~/.virtualenvs/pandaset-lv-custom/"; Lidarview
```
Or create a ENV variable in your `.bashrc` file, so it will be loaded in your session by default each time:
```
echo "export PV_VENV=\"~/.virtualenvs/pandaset-lv-custom/\"" >> ~/.bashrc
```

Now launch lidarview and check that the environment is working as intended in the python shell:
```
from paraview.web import venv
import mmdet3d
```

# Issues

If you have trouble installing `mmdetection3d`, read [their FAQ](https://mmdetection.readthedocs.io/en/latest/notes/faq.html).
For Lidarview check the [discourse forum](https://discourse.paraview.org/c/lidar/15), or create an issue in the [DeepLearningPlugins project](https://gitlab.kitware.com/LidarView/plugins/lidarviewdeeplearningplugins).
