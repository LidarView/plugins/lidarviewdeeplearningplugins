# =============================================================================
#
# Copyright (c) Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# =============================================================================

"""
This script is an example of how to run an mmdetection3d model in lidarview
"""

import os
import sys
import yaml
import paraview.simple as smp
import lidarviewcore.pipeline_setup_helpers as utils


with open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                       "terrestrial-3D-point-cloud-detection", "conf.yaml"), 'r') as fs:
    CONFIG = yaml.load(fs, Loader=yaml.SafeLoader)


def get_categories_map(config_file):
    print("WARNING: colormap from config file not implemented yet")
    categories_map = [
        '0', 'car',
        '1', 'truck',
        '2', 'bus',
        '3', 'other_vehicle',
        '4', 'bicycle',
        '5', 'motorcycle',
        '6', 'pedestrian',
        '7', 'cone',
        '8', 'sign',
        '9', 'construction_sign',
        '10', 'road_barrier',
        '11', 'construction_barrier']

    return categories_map


def apply_distance_th(data, dist_range, distance_array='distance_m'):
    # Remove closest points (eg vehicle reflection)
    threshold_data = smp.Threshold(Input=data)
    threshold_data.Scalars = ['POINTS', distance_array]
    threshold_data.LowerThreshold = dist_range[0]
    threshold_data.UpperThreshold = dist_range[-1]
    threshold_data.ThresholdMethod = "Between"
    threshold_data.UpdatePipeline()

    threshold_data = smp.ExtractSurface(Input=threshold_data)
    threshold_data.UpdatePipeline()

    return threshold_data


def apply_mmdetection_model(data, inference_params):
    detections = smp.MMDet3dInference(
        Input=data,
        ConfigFile=inference_params["config_file"],
        ModelWeightsFile=inference_params["weights_file"],
        DetectionThreshold=inference_params["detection_threshold"],
        UseIntensity=inference_params["use_intensity"],
        UseCenteredPointCloud=inference_params["use_centered_point_cloud"])

    return detections


def main():
    render_view = utils.setup_view(CONFIG['display']['use_eye_dome_lighting'])
    render_view.ViewSize = CONFIG["animation"]["resolution"]
    trajectory = utils.load_trajectory(CONFIG["io"]["trajectory_filename"])
    lidar_reader = utils.load_lidar_frames(
        CONFIG["io"]["lidar_filename"],
        CONFIG["io"]["calibration_filename"],
        CONFIG["io"]["lidar_interpreter"])
    lidar_data = apply_distance_th(lidar_reader, CONFIG["pipeline"]["distance_range"])

    trailing_frame = utils.aggregate_frames(
        lidar_data, trajectory,
        CONFIG["display"]["coloring"] == 'category',
        CONFIG["pipeline"]["nb_trailing_frames"],
        merge_frames=True,
        timestamp_array_name="timestamp")

    detections = apply_mmdetection_model(trailing_frame, CONFIG["pipeline"]["inference"])

    trailing_frame_display = smp.Show(trailing_frame, render_view)
    utils.setup_colormaps(
        CONFIG["display"]["coloring"],
        CONFIG["display"]["intensity_colormap_preset"],
        CONFIG["display"]["categories_config_filename"])

    smp.ColorBy(trailing_frame_display, ('POINTS', CONFIG["display"]["coloring"]))

    detections_display = smp.Show(detections, render_view)
    detections_display.LineWidth = 2.0

    categoryLUT = smp.GetColorTransferFunction('category')
    categoryLUT.Annotations = get_categories_map(CONFIG["pipeline"]["inference"]["config_file"])
    categoryLUT.InterpretValuesAsCategories = 1
    categoryLUT.ApplyPreset('KAAMS', True)

    smp.ColorBy(detections_display, ('FIELD', 'category'))
    detections_display.SetScalarBarVisibility(render_view, True)
    smp.Render(render_view)

    c_to_l = CONFIG["animation"]["camera_to_lidar_rotation"]

    if type(CONFIG["animation"]["camera"]) != list:
        CONFIG["animation"]["camera"] = [CONFIG["animation"]["camera"]]

    for ii, camera in enumerate(CONFIG["animation"]["camera"]):
        out_dir = os.path.join(CONFIG["io"]["output_directory"], camera.get("name", str(ii)))
        print("Saving animation images to: {}".format(out_dir))
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        animation = utils.setup_animation(
            trajectory,
            out_dir,
            CONFIG["animation"]["resolution"],
            camera_params=camera,
            camera_to_lidar_rotation=(c_to_l["axes"], c_to_l['values']),
            animation_start_step=CONFIG["animation"]["start_time"],
            animation_end_step=CONFIG["animation"]["end_time"])
        if CONFIG["dry_run"]:
            smp.Interact(render_view)
        else:
            animation.Play()


if __name__ == "__main__":

    lvsb_python_path = [p for p in sys.path if (("lvsb" in p) & ("install" in p))][0]
    lvsb_path = os.path.join(lvsb_python_path, "..", "..", "lidarview", "plugins") # TODO get from cmake configure instead using .in file
    lidarcore_plugin_path = os.path.join(f"{lvsb_path}", "LidarCorePlugin", "LidarCorePlugin.so")
    velodyne_plugin_path = os.path.join(f"{lvsb_path}", "VelodynePlugin", "VelodynePlugin.so")
    mmdet3d_plugin_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "MMDetection3DInference.py")
    smp.LoadPlugin(lidarcore_plugin_path, remote=False)
    smp.LoadPlugin(velodyne_plugin_path, remote=False)
    smp.LoadPlugin(mmdet3d_plugin_path, remote=False)

    main()


if __name__ == "__vtkconsole__":

    main()
