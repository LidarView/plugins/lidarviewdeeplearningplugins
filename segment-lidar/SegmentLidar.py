# =============================================================================
#
# Copyright (c) Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# =============================================================================

"""
This module contains a LidarView filter that performs semantic segmentation on a
polydata aerial pointcloud using segment-lidar (segment-anything backend).
This requires segment-lidar installed on the python used by LidarView.
See https://gitlab.kitware.com/keu-computervision/ml/segment-lidar
"""

import os
import tempfile

from paraview.web import venv
import numpy as np
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.numpy_interface import dataset_adapter
from vtkmodules.vtkCommonDataModel import vtkPolyData
from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain, smhint
import torch

try: 
    import segment_lidar.samlidar
except ImportError as e:
    print("""segment_lidar module could not be loaded.
        Please install segment_lidar by following the documentation
        (https://gitlab.kitware.com/LidarView/plugins/lidarviewdeeplearningplugins/-/blob/main/segment-lidar/INSTALL.md)""")
    raise e

@smproxy.filter(name="SegmentLidar")
@smproperty.input(name="Input")
@smdomain.datatype(dataTypes=["vtkPolyData"], composite_data_supported=False)
class SegmentLidar(VTKPythonAlgorithmBase):
    """Perform semantic segmentation on aerial pointcloud using segment-lidar (with segment-anything backend)."""
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self,
                                        nInputPorts=1,
                                        nOutputPorts=1,
                                        outputType="vtkPolyData")
        self.chpkt = None
        self.resolution = 0.25
        self.model = None
        self.use_gpu = False
        self.gpu_id = 0
        self.device = "cpu"
        self.tmpdir = tempfile.mkdtemp()

    # def RequestDataObject(self, request, inInfo, outInfo):
    #     inData = self.GetInputData(inInfo, 0, 0)
    #     outData = self.GetOutputData(outInfo, 0)
    #     assert inData is not None
    #     if outData is None or (not outData.IsA(inData.GetClassName())):
    #         outData = inData.NewInstance()
    #         outInfo.GetInformationObject(0).Set(outData.DATA_OBJECT(), outData)

    #     return super().RequestDataObject(request, inInfo, outInfo)

    def RequestData(self, request, inInfo, outInfo):
        """Main process"""
        input = dataset_adapter.WrapDataObject(self.GetInputData(inInfo, 0, 0))
        output = dataset_adapter.WrapDataObject(self.GetOutputData(outInfo, 0))
        # keep input data mostly to preserve cell points
        output.ShallowCopy(input.VTKObject)

        #check if model is initialized
        if self.model:
            # Concatenate points and colors and cast to float64
            color_info = np.iinfo(input.PointData["Color"].dtype)
            points_dtype = input.Points.dtype
            max_uint8 = np.iinfo(np.uint8).max
            colors = max_uint8*(input.PointData["Color"]/color_info.max)
            colors = np.array(colors, dtype=points_dtype)
            in_points = np.zeros((len(input.Points), 6), dtype=points_dtype)
            in_points[:, :3] = input.Points
            in_points[:, 3:] = colors
            # Extract ground
            cloud, non_ground, ground = self.model.csf(in_points)
            tmp_image_path = os.path.join(self.tmpdir, "raster.tif")
            tmp_labels_path = os.path.join(self.tmpdir, "labeled.tif")
            # Segmentation
            labels, *_ = self.model.segment(
                points=cloud, image_path=tmp_image_path, labels_path=tmp_labels_path)
            indices = np.concatenate((non_ground, ground))
            segment_ids = np.append(labels, np.full(len(ground), -1))
            output.Points = input.Points[indices]
            output.PointData.append(segment_ids, "SegmentLidarId")

        return 1
    
    def _load_model(self):
        """Load the ViT model given its pytorch weights file"""
        if self.chpkt:
            model_type_idx = self.chpkt.find("vit_")
            model_type = self.chpkt[model_type_idx:model_type_idx+5]
            if self.device == "cpu":
                # force disabling gpu because samgeo ignores device
                os.environ["CUDA_VISIBLE_DEVICES"] = ""
            self.model = segment_lidar.samlidar.SamLidar(
                self.chpkt, model_type=model_type, resolution=self.resolution, device=self.device)
            print(f"Reading model {self.model}")

    @smproperty.intvector(name="GpuId", default_values=0)
    @smdomain.intrange(min=0, max=torch.cuda.device_count()-1)
    def SetGpuId(self, gpu_id):
        """Set the gpu ID to be used if multiple GPUs are available."""
        self.gpu_id = gpu_id
        if self.model:
            self._load_model()
        self.Modified()

    @smproperty.doublevector(name="Resolution", default_values=0.25)
    @smdomain.doublerange(min=0)
    def SetResolution(self, resolution):
        """Set the resolution (meters per px) for the rasterized image"""
        self.resolution  = resolution
        self.Modified()

    @smproperty.xml("""
        <IntVectorProperty name="UseGpu"
            default_values="0"
            command="SetUseGpu">
            help="Whether to use GPU or not."
            <BooleanDomain name='bool'/>
        </IntVectorProperty>
        """)
    def SetUseGpu(self, use_gpu):
        """Whether to use the GPU or not"""
        if bool(use_gpu):
            self.device = f"cuda:{self.gpu_id}"
        else:
            self.device = "cpu"
        if self.model:
            self._load_model()
        self.Modified()

    @smproperty.stringvector(name="CheckpointFile", default_values="")
    @smdomain.filelist()
    @smhint.filechooser(extensions="pth", file_description="ViT model weights file")
    def SetModelWeights(self, filepath):
        """Load the model weights specified by the filename"""
        if filepath:
            self.chpkt = filepath
            self._load_model()
        self.Modified()
