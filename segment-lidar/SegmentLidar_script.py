# trace generated using paraview version 5.11.0-881-gb5bd6d8bef
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 11

#### import the simple module from the paraview
import paraview.simple as smp

# get active view
view = smp.GetActiveViewOrCreate("RenderView")
cam = view.GetActiveCamera()

reader = smp.OpenDataFile("~/Documents/localdata/ParaViewTestingDataFiles-v5.11.1/Testing/Data/can.ex2")
smp.Show(reader)

# get active source.
a1845_5177_v1_4_segmentedlas = GetActiveSource()

# set active source
SetActiveSource(a1845_5177_v1_4_segmentedlas)

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
a1845_5177_v1_4_segmentedlasDisplay = Show(a1845_5177_v1_4_segmentedlas, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
a1845_5177_v1_4_segmentedlasDisplay.Representation = 'Surface'

# reset view to fit data
renderView1.ResetCamera(False, 0.9)

# hide data in view
Hide(a1845_5177_v1_4_segmentedlas, renderView1)

# show data in view
a1845_5177_v1_4_segmentedlasDisplay = Show(a1845_5177_v1_4_segmentedlas, renderView1, 'GeometryRepresentation')

# reset view to fit data
renderView1.ResetCamera(False, 0.9)

# reset view to fit data bounds
renderView1.ResetCamera(1845000.0, 1846000.0, 5177000.0, 5178000.0, 0.46, 713.67, True, 0.9)

# set scalar coloring
ColorBy(a1845_5177_v1_4_segmentedlasDisplay, ('POINTS', 'segment_id'))

# rescale color and/or opacity maps used to include current data range
a1845_5177_v1_4_segmentedlasDisplay.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
a1845_5177_v1_4_segmentedlasDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'segment_id'
segment_idLUT = GetColorTransferFunction('segment_id')

# get opacity transfer function/opacity map for 'segment_id'
segment_idPWF = GetOpacityTransferFunction('segment_id')

# get 2D transfer function for 'segment_id'
segment_idTF2D = GetTransferFunction2D('segment_id')

# create new layout object 'Layout #2'
layout2 = CreateLayout(name='Layout #2')

# set active view
SetActiveView(None)

# Create a new 'Render View'
renderView2 = CreateView('RenderView')
renderView2.AxesGrid = 'GridAxes3DActor'
renderView2.StereoType = 'Crystal Eyes'
renderView2.CameraFocalDisk = 1.0

# assign view to a particular cell in the layout
AssignViewToLayout(view=renderView2, layout=layout2, hint=0)

# show data in view
a1845_5177_v1_4_segmentedlasDisplay_1 = Show(a1845_5177_v1_4_segmentedlas, renderView2, 'GeometryRepresentation')

# trace defaults for the display properties.
a1845_5177_v1_4_segmentedlasDisplay_1.Representation = 'Surface'

# reset view to fit data
renderView2.ResetCamera(False, 0.9)

# set scalar coloring
ColorBy(a1845_5177_v1_4_segmentedlasDisplay_1, ('POINTS', 'Color', 'Magnitude'))

# rescale color and/or opacity maps used to include current data range
a1845_5177_v1_4_segmentedlasDisplay_1.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
a1845_5177_v1_4_segmentedlasDisplay_1.SetScalarBarVisibility(renderView2, True)

# get color transfer function/color map for 'Color'
colorLUT = GetColorTransferFunction('Color')

# get opacity transfer function/opacity map for 'Color'
colorPWF = GetOpacityTransferFunction('Color')

# get 2D transfer function for 'Color'
colorTF2D = GetTransferFunction2D('Color')

# Properties modified on a1845_5177_v1_4_segmentedlasDisplay_1
a1845_5177_v1_4_segmentedlasDisplay_1.MapScalars = 0

# set scalar coloring
ColorBy(a1845_5177_v1_4_segmentedlasDisplay_1, ('POINTS', 'Intensity'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(colorLUT, renderView2)

# rescale color and/or opacity maps used to include current data range
a1845_5177_v1_4_segmentedlasDisplay_1.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
a1845_5177_v1_4_segmentedlasDisplay_1.SetScalarBarVisibility(renderView2, True)

# get color transfer function/color map for 'Intensity'
intensityLUT = GetColorTransferFunction('Intensity')

# get opacity transfer function/opacity map for 'Intensity'
intensityPWF = GetOpacityTransferFunction('Intensity')

# get 2D transfer function for 'Intensity'
intensityTF2D = GetTransferFunction2D('Intensity')

# set scalar coloring
ColorBy(a1845_5177_v1_4_segmentedlasDisplay_1, ('POINTS', 'Color', 'Magnitude'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(intensityLUT, renderView2)

# rescale color and/or opacity maps used to include current data range
a1845_5177_v1_4_segmentedlasDisplay_1.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
a1845_5177_v1_4_segmentedlasDisplay_1.SetScalarBarVisibility(renderView2, True)

# Properties modified on a1845_5177_v1_4_segmentedlasDisplay_1
a1845_5177_v1_4_segmentedlasDisplay_1.MapScalars = 1

# Properties modified on a1845_5177_v1_4_segmentedlasDisplay_1
a1845_5177_v1_4_segmentedlasDisplay_1.MapScalars = 0

# show data in view
a1845_5177_v1_4_segmentedlasDisplay_1 = Show(a1845_5177_v1_4_segmentedlas, renderView2, 'GeometryRepresentation')

# reset view to fit data
renderView2.ResetCamera(False, 0.9)

# show color bar/color legend
a1845_5177_v1_4_segmentedlasDisplay_1.SetScalarBarVisibility(renderView2, True)

# update the view to ensure updated data information
renderView2.Update()

# create a new 'PDAL Reader'
a1845_5177_v1_4las = PDALReader(registrationName='1845_5177_v1_4.las', FileName='~/Documents/localdata/lidar-datasets/grandlyon/1845_5177_v1_4.las')

# hide data in view
Hide(a1845_5177_v1_4_segmentedlas, renderView2)

# set active source
SetActiveSource(a1845_5177_v1_4las)

# show data in view
a1845_5177_v1_4lasDisplay = Show(a1845_5177_v1_4las, renderView2, 'GeometryRepresentation')

# trace defaults for the display properties.
a1845_5177_v1_4lasDisplay.Representation = 'Surface'

# reset view to fit data
renderView2.ResetCamera(False, 0.9)

# hide data in view
Hide(a1845_5177_v1_4las, renderView2)

#================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
#================================================================

# get layout
layout1 = GetLayoutByName("Layout #1")

#--------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(860, 811)

# layout/tab size in pixels
layout2.SetSize(860, 811)

#-----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.CameraPosition = [1845500.0, 5177500.0, 3416.8817129174604]
renderView1.CameraFocalPoint = [1845500.0, 5177500.0, 357.065]
renderView1.CameraViewAngle = 22.975750102753803
renderView1.CameraParallelScale = 791.938839826031

# current camera placement for renderView2
renderView2.CameraPosition = [0.0, 0.0, 6.6921304299024635]
renderView2.CameraParallelScale = 1.7320508075688772

#--------------------------------------------
# uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).



#--------------------------------------------
#--------------------------------------------
# Reading segment-lidar output
#--------------------------------------------
#--------------------------------------------


# trace generated using paraview version 5.11.0-881-gb5bd6d8bef
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'PDAL Reader'
a1845_5177_recentered_w_rgb_segmentedlas = PDALReader(registrationName='1845_5177_recentered_w_rgb_segmented.las', FileName='~/Documents/localdata/lidar-datasets/grandlyon/segment-lidar/1845_5177_recentered_w_rgb_segmented.las')

# set active source
SetActiveSource(a1845_5177_recentered_w_rgb_segmentedlas)

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
a1845_5177_recentered_w_rgb_segmentedlasDisplay = Show(a1845_5177_recentered_w_rgb_segmentedlas, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
a1845_5177_recentered_w_rgb_segmentedlasDisplay.Representation = 'Surface'

# reset view to fit data
renderView1.ResetCamera(False, 0.9)

# set scalar coloring
ColorBy(a1845_5177_recentered_w_rgb_segmentedlasDisplay, ('POINTS', 'segment_id'))

# rescale color and/or opacity maps used to include current data range
a1845_5177_recentered_w_rgb_segmentedlasDisplay.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
a1845_5177_recentered_w_rgb_segmentedlasDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'segment_id'
segment_idLUT = GetColorTransferFunction('segment_id')

# get opacity transfer function/opacity map for 'segment_id'
segment_idPWF = GetOpacityTransferFunction('segment_id')

# get 2D transfer function for 'segment_id'
segment_idTF2D = GetTransferFunction2D('segment_id')

#================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
#================================================================

# get layout
layout1 = GetLayout()

#--------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(860, 811)

#-----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.CameraPosition = [337.1993680666398, 644.1727715731867, 3409.1199450772883]
renderView1.CameraFocalPoint = [499.995, 499.995, 357.065]
renderView1.CameraViewUp = [0.007231443601976402, 0.998878063957625, -0.04680085007452515]
renderView1.CameraParallelScale = 791.9325262135658

#--------------------------------------------
# uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).



