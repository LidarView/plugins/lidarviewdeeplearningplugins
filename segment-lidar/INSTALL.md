# Segment-lidar 

compatible only with python 3.9

# Create a new virtual env

```
python3 -m venv /path/to/segment-lidar
source /path/to/segment-lidar/bin/activate && cd /path/to/segment-lidar
```

## Build libproj from source

Check which libproj version LidarView is using, it should be `9.0.1`.
We will now compile this specific version of libproj.
```
git clone https://github.com/OSGeo/PROJ
cd PROJ && git checkout 9.0.1 && mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX="/path/to/segment-lidar/PROJ" ..
cmake --build . && cmake --install .
```

## Install pyproj and segment-lidar
We can now compile the python wrapper `pyproj` using our installed `libproj`.

> **Note**:
> We are not using `libproj` under the LidarView installation directory because it is missing the binaries.

```
cd /path/to/segment-lidar
pip install --upgrade cython
git clone https://github.com/pyproj4/pyproj.git
cd pyproj && git checkout 3.5.0
git cherry-pick -n 1452ba404be58c14a6b64d4551c320022f5aafcf
export PROJ_DIR="/path/to/segment-lidar/PROJ" && pip install .
```
And finally we install our custom fork of [segment-lidar](https://gitlab.kitware.com/keu-computervision/ml/segment-lidar)
```
pip install segment-lidar@git+https://gitlab.kitware.com/keu-computervision/ml/segment-lidar.git@main
```

# Downloading the model

The model checkpoint from the Meta AI group is available at this adress:
https://github.com/facebookresearch/segment-anything#model-checkpoints

Please select the lowest resolution `vit_b` if you have performance issues, it should use no more than 4GB of RAM.
